# INDICADORES REST API

API possibilita a leitura da lista de indicados e vencedores da categoria Pior Filme do Golden Raspberry Awards.

## Ambiente do desenvolvedor

 - Java 8
 - Spring Tools for Eclipse (recomendado mas pode-se utilizar qualquer uma IDE que suporte Java)
 
 Fazer checkou desse projeto e importe para sua IDE, as dependecias serao baixadas automaticamente pelo maven.
 
 
 src/main/resources
 
 - SCHEMA.SQL É utlizado ao iniciar aplicação, podemos melhorar criando tabelas para separar filmes, producers e utilizar join 			  no select.
 
 Informações H2:
> 
	spring.datasource.url=jdbc:h2:file:./data/exemplo 
	spring.datasource.driverClassName=org.h2.Driver
	spring.datasource.username=sa
	spring.datasource.password=
	spring.jpa.database-platform=org.hibernate.dialect.H2Dialect
	
  	
# Links aplicação
  - Banco H2 - http://localhost:8080/h2/login.do
  - Swagger - http://localhost:8080/swagger-ui.html#/
  - Controller - http://localhost:8080/api/filmes/indicados
  
# Links Documentação
 https://commons.apache.org/proper/commons-csv/user-guide.html
 
   
  .......em construção 
  - Controller que irá retornar Max e Min
  - Query para filtrar informações
  - README.md
