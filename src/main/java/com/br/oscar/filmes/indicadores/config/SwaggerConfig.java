package com.br.oscar.filmes.indicadores.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

import com.google.common.collect.Sets;

import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@EnableSwagger2
public class SwaggerConfig {

	@Bean
	public Docket api() {
		return new Docket(DocumentationType.SWAGGER_2)
				.useDefaultResponseMessages(false)
				.produces(Sets.newHashSet("application/json"))
				.select()
				.apis(RequestHandlerSelectors.basePackage("com.br.oscar.filmes.indicadores.controller"))
				.paths(PathSelectors.any())
				.build()
				.apiInfo(this.apiInfo());

	}

	private ApiInfo apiInfo() {

		return new ApiInfoBuilder()
				.title("INDICADORES REST API")
				.description("API possibilita a leitura da lista de indicados e vencedores da categoria Pior Filme do Golden Raspberry Awards.")
				.version("0.0.1")
				.license("Apache License Version 2.0").licenseUrl("http://www.oscar.com")
				.build();
	}

}
