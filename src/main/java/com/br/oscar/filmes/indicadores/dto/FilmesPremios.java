package com.br.oscar.filmes.indicadores.dto;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class FilmesPremios {

	private List<PremiosIndicados> min;
	private List<PremiosIndicados> max;

}
