package com.br.oscar.filmes.indicadores.config;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.br.oscar.filmes.indicadores.service.CsvService;


@Component
public class IndicadoresConfig {

	@Autowired
	private CsvService csvService;

	/**
	 * metodo responsavel por ler o arquivo .csv e inserir na base h2 apos o projeto iniciar. 
	 */
	@PostConstruct
	public void postConstruct() {
		csvService.lerArquivoCsv();
	}

}
