package com.br.oscar.filmes.indicadores.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.br.oscar.filmes.indicadores.dto.FilmesPremios;
import com.br.oscar.filmes.indicadores.repository.PremiosRepository;

import io.swagger.annotations.Api;

@RestController
@RequestMapping("/api")
@Api(tags = { "IndicadoresController" })
public class IndicadoresController {

	@SuppressWarnings("unused")
	@Autowired
	private PremiosRepository premiosRepository;
	
	@GetMapping("/filmes/indicados")
    public ResponseEntity<?>  findByMinAndMax(){
		
		List<FilmesPremios> listaFilmesPremios = new ArrayList<FilmesPremios>();
	
		//TODO criar regras para Min e Max e Query
		
		return ResponseEntity.status(HttpStatus.OK).body(listaFilmesPremios);
       
    }
}
