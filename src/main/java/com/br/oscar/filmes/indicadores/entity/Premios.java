package com.br.oscar.filmes.indicadores.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "TBL_INDICACAO")
public class Premios implements Serializable {

	private static final long serialVersionUID = 1L;

	@GeneratedValue
	@Id
	@JsonIgnore
	private int id;

	@Column(name = "YEAR")
	private String year;
	
	@Column(name = "TITLE")
	private String title;
	
	@Column(name = "STUDIOS")
	private String studios;
	
	@Column(name = "PRODUCERS")
	private String producers;
	
	@Column(name = "WINNER")
	private String winner;

	public Premios(String year, String title, String studios, String producers, String winner) {
		super();
		this.year = year;
		this.title = title;
		this.studios = studios;
		this.producers = producers;
		this.winner = winner;
	}
}
