package com.br.oscar.filmes.indicadores.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.br.oscar.filmes.indicadores.entity.Premios;

@Repository
@Transactional(readOnly = true)
public interface PremiosRepository  extends JpaRepository<Premios, Long>{

	@Query(value = "SELECT * FROM TBL_INDICACAO where WINNER = :yes order by year,producers",nativeQuery = true)
	public List<Premios> findByWinner(@Param("yes") String yes);
}
