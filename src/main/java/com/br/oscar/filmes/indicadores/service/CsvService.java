package com.br.oscar.filmes.indicadores.service;

import java.io.FileReader;
import java.io.Reader;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.br.oscar.filmes.indicadores.entity.Premios;
import com.br.oscar.filmes.indicadores.repository.PremiosRepository;



@Service
public class CsvService {
	
	@Autowired
	private PremiosRepository premiosRepository;

	@Value("${pathCsv}")
	private String caminhoArquivo;
	
	@Value("${movieList}")
	private String nomeArquivo;
	
	public static final Logger logger = Logger.getLogger("Log");
	
	public void lerArquivoCsv(){		
		
		logger.info("Lendo arquivo Csv.");
		
		try {
			
			//Ficou complexo pegar cada linha/coluna e validar todas.
			//BufferedReader reader = new BufferedReader(new FileReader(caminhoArquivo + nomeArquivo));
						
			Reader in = new FileReader(caminhoArquivo + nomeArquivo);
					
			Iterable<CSVRecord> records = CSVFormat.RFC4180.withHeader("year", "title", "studios","producers","winner")
														   .withDelimiter(';')
														   .parse(in);
			
			List<Premios> listaPremios = new ArrayList<Premios>();
			
			for (CSVRecord csvRecord : records) {
				//vamos pular a primeira linha que eh o Header.
				if (csvRecord.getRecordNumber() == 1) {
					continue;
				}

				setLinhasPremios(csvRecord, listaPremios);	
			}
			
			gravaCsv(listaPremios);	
			
		} catch (Exception ex) {
			logger.info("Erro ao ler o arquivo de entrada .CSV" + ex.getMessage());
		}
	
	}

	private void setLinhasPremios(CSVRecord csvRecord, List<Premios> listaPremios ) {
		
		String year = csvRecord.get("year");
		String title = csvRecord.get("title");
		String studios = csvRecord.get("studios");
		String produc = csvRecord.get("producers");
		String winner = csvRecord.get("winner");
		
		listaPremios.add(new Premios(year, title, studios, produc, winner));
		
	}

	public void gravaCsv(List<Premios> premiosFilme){
				
			premiosRepository.saveAll(premiosFilme);
				
	}

}
